#!/usr/bin/env bash

git remote add upstream git@github.com:spantaleev/matrix-docker-ansible-deploy.git
git fetch upstream master:upstream
git merge upstream --no-edit
git push

git switch upstream
git push --set-upstream origin upstream

git switch main
sleep 5

echo "Enter tag name:"
read
git tag $REPLY
git push origin $REPLY
